import testinfra


def test_os_release(host):
    assert host.file("/etc/os-release").contains("Alpine Linux")


def test_nginx_exist(host):
    nginx_package = host.package("nginx")
    assert nginx_package.is_installed is True
    assert nginx_package.version.startswith("1.18.")





